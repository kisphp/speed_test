<?php

require_once 'vendor/autoload.php';

use Faker\Provider\en_US\Text;
use Faker\Generator;

$db = new mysqli('localhost', 'speed_test', 'speed_test', 'speed_test');

$faker = Faker\Factory::Create();
$faker->addProvider(new Text($faker));

$sql = "DROP TABLE `articles` IF EXISTS";
$db->query($sql);

$sql = "CREATE TABLE `articles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `content` TEXT NULL,
  PRIMARY KEY (`id`)
);";
$db->query($sql);

for ($i=1; $i<=100; $i++) {
    $sql = sprintf(
        "INSERT INTO articles SET title = '%s', content = '%s'",
        $faker->text(rand(10, 30)),
        $faker->text(rand(20, 300))
    );
    $db->query($sql);
    echo 'Added id: '.$db->insert_id;
    echo "\n";
}



