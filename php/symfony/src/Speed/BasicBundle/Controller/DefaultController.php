<?php

namespace Speed\BasicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SpeedBasicBundle:Default:index.html.twig');
    }

    /**
     * @Template()
     */
    public function articlesAction($keyword)
    {
        $repo = $this->getDoctrine()->getRepository('SpeedBasicBundle:Articles');
        if ( ! empty($keyword) ) {
            $query = $repo->createQueryBuilder('a')
                ->where('a.content LIKE :key')
                ->setParameter('key', '%'.$keyword.'%')
                ->getQuery()
            ;
            $articles = $query->getResult();
        } else {
            $articles = $repo->findAll();
        }

        return array(
            'articles' => $articles,
        );
    }
}
