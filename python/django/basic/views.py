from django.shortcuts import render

from .models import Articles

def articles(request, keyword=''):
    if keyword != '':
        db_articles = Articles.objects.filter(content__icontains=keyword)
    else:
        db_articles = Articles.objects.all()

    return render(request, 'basic/articles.html', {
        'articles': db_articles,
    })

def home(request):
    mini = Articles.objects.filter(content__startswith='minim')

    return render(request, 'basic/home.html')

