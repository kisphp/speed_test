from django.db import models

# https://docs.djangoproject.com/en/1.7/topics/db/queries/

class Articles(models.Model):
    title = models.IntegerField(max_length=255)
    content = models.TextField()

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'articles'