from django.conf.urls import patterns, include, url
#from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'basic.views.home', name='home'),
    url(r'^articles$', 'basic.views.articles', name='articles'),
    url(r'^articles/(?P<keyword>\w+)$', 'basic.views.articles', name='articles_key'),
)
